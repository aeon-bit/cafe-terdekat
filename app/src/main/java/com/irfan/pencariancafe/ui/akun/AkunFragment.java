package com.irfan.pencariancafe.ui.akun;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.MainActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.databinding.FragmentAkunBinding;
import com.irfan.pencariancafe.utils.ApiClient;
import com.irfan.pencariancafe.utils.SessionManager;

import de.hdodenhof.circleimageview.CircleImageView;


public class AkunFragment extends Fragment {

    private FragmentAkunBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentAkunBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        CardView cv_btnGoLogin = root.findViewById(R.id.cv_btnGoLogin);
        CardView cv_btnPerformLogout = root.findViewById(R.id.cv_btnPerformLogout);

        if(SessionManager.isLogin()){
            cv_btnGoLogin.setVisibility(View.INVISIBLE);
            cv_btnPerformLogout.setVisibility(View.VISIBLE);
            setcalues(root);
        } else {
            cv_btnGoLogin.setVisibility(View.VISIBLE);
            cv_btnPerformLogout.setVisibility(View.INVISIBLE);
        }

        cv_btnGoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginRegisterActivity.class);
                startActivity(intent);
            }
        });

        cv_btnPerformLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.logout();

                Toast toast = Toast.makeText(getActivity(), "Logout Berhasil", Toast.LENGTH_SHORT);
                View view = toast.getView();
                view.setPadding(42, 8, 42, 8);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();

                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

    private void setcalues(View root) {
        TextView tv_namaAkun = root.findViewById(R.id.tv_namaAkun);
        TextView tv_usernameAkun = root.findViewById(R.id.tv_usernameAkun);
        TextView tv_passAkun = root.findViewById(R.id.tv_passAkun);
        CircleImageView icv_fotoAkun = root.findViewById(R.id.icv_fotoAkun);

        tv_namaAkun.setText(": " + SessionManager.getNamaUser());
        tv_usernameAkun.setText(": " + SessionManager.getUsername());
        tv_passAkun.setText(": " + SessionManager.getPassword());

        Glide.with(getContext()).load(ApiClient.IMAGE_URL + SessionManager.getFoto())
                .error(R.mipmap.ic_launcher).into(icv_fotoAkun);

        Log.d("akun", "setcalues: " + SessionManager.getFoto());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}