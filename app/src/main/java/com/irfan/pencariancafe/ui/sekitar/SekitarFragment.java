package com.irfan.pencariancafe.ui.sekitar;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.adapter.AdapterListCafeHome;
import com.irfan.pencariancafe.auth.LoginFragment;
import com.irfan.pencariancafe.databinding.FragmentSekitarBinding;
import com.irfan.pencariancafe.models.Cafes;
import com.irfan.pencariancafe.ui.admincontrol.TambahCafeFragment;
import com.irfan.pencariancafe.utils.ApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.irfan.pencariancafe.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SekitarFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double selfLat, selfLng;

    List<Cafes> listCafes;
    List<Cafes> listCafesFiltered;
    RecyclerView rv_sekitar;
    RecyclerView.Adapter rvAdapter;
    RecyclerView.LayoutManager rvLayoutManager;
    RequestQueue requestQueue;

    private FragmentSekitarBinding binding;

    private FusedLocationProviderClient fusedLocationProviderClient;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentSekitarBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        rv_sekitar = root.findViewById(R.id.rv_sekitar);
        requestQueue = Volley.newRequestQueue(getContext());
        listCafes = new ArrayList<>();
        listCafesFiltered = new ArrayList<>();
//        requestAllCafe();
        rvLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_sekitar.setLayoutManager(rvLayoutManager);
        rvAdapter = new AdapterListCafeHome(listCafesFiltered, getContext());
        rv_sekitar.setAdapter(rvAdapter);

        //init map
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_sekitar);
        mapFragment.getMapAsync(this);
        requestPermission();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        CardView cv_currentLocation = root.findViewById(R.id.cv_currentLocation);
        cv_currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = new LatLng(selfLat, selfLng);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        FloatingActionButton fab_addCafe = root.findViewById(R.id.fab_addCafe);
        if (SessionManager.isLogin() && SessionManager.getRole().equals("admin")){
            fab_addCafe.setVisibility(View.VISIBLE);
        } else {
            fab_addCafe.setVisibility(View.INVISIBLE);
        }
        fab_addCafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_activity_main, new TambahCafeFragment()).commit();
            }
        });

        return root;
    }

    private void requestAllCafe() {
        String apiUrl = ApiClient.API + "getAll_cafeHome.php";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, apiUrl, null,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(JSONArray response) {

                        //get Json
                        Log.d("respon", "resto: " + response.toString());
//                        if(response.length() < 1){
//                            tv_alert.setVisibility(View.VISIBLE);
//                        } else {
//                            tv_alert.setVisibility(View.INVISIBLE);
//                        }
                        //extract datas
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject data = response.getJSONObject(i);
                                //get key 1 by 1
                                Cafes model = new Cafes();

                                model.setId_cafe(data.getString("id_cafe"));
                                model.setNama_cafe(data.getString("nama_cafe"));
                                model.setAlamat(data.getString("alamat"));
                                model.setLat(data.getString("lat"));
                                model.setLng(data.getString("lng"));
                                model.setJam_buka(data.getString("jam_buka"));
                                model.setJam_tutup(data.getString("jam_tutup"));
                                model.setFoto(data.getString("foto"));
                                model.setRating(data.getString("rating"));
                                model.setJarak(getJarak(data.getString("lat"), data.getString("lng")));

                                //input model to list if jarak < 5 km
                                if (Double.parseDouble(getJarak(data.getString("lat"), data.getString("lng"))) < 5.00) {
                                    listCafesFiltered.add(model);
                                    listCafes.add(model);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            rvAdapter.notifyDataSetChanged();
                        }

                        Collections.sort(listCafesFiltered, (a, b) -> Double.compare(a.getJarakD(), b.getJarakD()));
                        Collections.sort(listCafes, (a, b) -> Double.compare(a.getJarakD(), b.getJarakD()));

                        rvAdapter.notifyDataSetChanged();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("json_respon", "ERROR : " + error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayRequest);
    }

    private String getJarak(String lat, String lng) {
        double radian = 0.0174533;
        double radLat1 = Double.valueOf(selfLat * radian);
        double radLat2 = Double.valueOf(lat) * radian;
        double radLng1 = Double.valueOf(selfLng * radian);
        double radLng2 = Double.valueOf(lng) * radian;

//        =======
        //Delta radian pos 1 - pos 2
        BigDecimal delRadLat = BigDecimal.valueOf(radLat2).subtract(BigDecimal.valueOf(radLat1));
        BigDecimal delRadLng = BigDecimal.valueOf(radLng2).subtract(BigDecimal.valueOf(radLng1));

//        =======
//        Val a
        double valA = Math.sin(Math.pow((delRadLat.doubleValue() / 2), 2));
//        Val c
        double valC = Math.sin(Math.pow((delRadLng.doubleValue() / 2), 2));

//        =====
//        Val D
        double valD = 2 * 6371 * Math.asin(Math.sqrt(valA + valC));

//        =====
        String jarak = String.valueOf(valD);
//        Log.d("posisi", "jarak: " + jarak);


//        double gotJarak = 0.0;
        return String.valueOf(jarak);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getLastLocation();

//        LatLng pos = new LatLng(-7.773401782689675, 110.41982022912433);
//        mMap.addMarker(new MarkerOptions().position(pos).title("Posisi saat ini"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16.0f));
//        mMap.animateCamera(CameraUpdateFactory.newLatLng(pos));
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    //myLocation = location.toString();
                    selfLat = location.getLatitude();
                    selfLng = location.getLongitude();
                }

                Log.d("lokasi", "Latitude: " + selfLat);
                Log.d("lokasi", "Longitude: " + selfLng);

                requestAllCafe();
                setMapMark();
            }
        });
    }

    private void setMapMark() {
        LatLng latLng = new LatLng(selfLat, selfLng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOptions.title("Lokasi kamu");
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.addMarker(markerOptions);
    }
}