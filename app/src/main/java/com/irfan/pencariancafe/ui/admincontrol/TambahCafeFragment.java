package com.irfan.pencariancafe.ui.admincontrol;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static android.os.Environment.getExternalStoragePublicDirectory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.irfan.pencariancafe.BuildConfig;
import com.irfan.pencariancafe.MainActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.databinding.FragmentTambahCafeBinding;
import com.irfan.pencariancafe.models.ResponseModel;
import com.irfan.pencariancafe.utils.ApiClient;
import com.irfan.pencariancafe.utils.ApiInterface;
import com.kosalgeek.android.photoutil.ImageBase64;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TambahCafeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, AdapterView.OnItemSelectedListener {
    private GoogleMap mMap;
    private double selfLat, selfLng;

    String pathToFile, encodeImage, newLat, newLng;

    ImageView iv_fotoTambah;
    EditText save_latTambah, save_lngTambah;

    private FusedLocationProviderClient fusedLocationProviderClient;

    private FragmentTambahCafeBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentTambahCafeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //init map
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_tambah);
        mapFragment.getMapAsync(this);
        requestPermission();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        //spinner jam
        Spinner sp_jamBuka = root.findViewById(R.id.sp_jamBuka);
        Spinner sp_jamTutup = root.findViewById(R.id.sp_jamTutup);
        ArrayAdapter<CharSequence> adapterJam = ArrayAdapter.createFromResource(getContext(), R.array.jam, android.R.layout.simple_spinner_item);
        adapterJam.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp_jamBuka.setAdapter(adapterJam);
        sp_jamTutup.setAdapter(adapterJam);

        sp_jamBuka.setOnItemSelectedListener(this);
        sp_jamTutup.setOnItemSelectedListener(this);

        //foto
        iv_fotoTambah = root.findViewById(R.id.iv_fotoTambah);
        iv_fotoTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihFotoFromGallery();
            }
        });

        EditText et_namaTambah = root.findViewById(R.id.et_namaTambah);
        EditText et_alamatTambah = root.findViewById(R.id.et_alamatTambah);
        save_latTambah = root.findViewById(R.id.save_latTambah);
        save_lngTambah = root.findViewById(R.id.save_lngTambah);

        CardView cv_btnPerformTambahData = root.findViewById(R.id.cv_btnPerformTambahData);
        cv_btnPerformTambahData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_namaTambah.getText().toString().equals("")){
                    et_namaTambah.setError("Masukkan nama");
                } else if (et_alamatTambah.getText().toString().equals("")){
                    et_alamatTambah.setError("Masukkan alamat");
                } else if (save_latTambah.getText().toString().equals("")){
                    save_latTambah.setError("Masukkan lokasi");
                } else if (save_lngTambah.getText().toString().equals("")){
                    save_lngTambah.setError("Masukkan lokasi");
                } else {
                    performTambahData(
                            et_namaTambah.getText().toString(),
                            et_alamatTambah.getText().toString(),
                            sp_jamBuka.getSelectedItem().toString(),
                            sp_jamTutup.getSelectedItem().toString(),
                            encodeImage,
                            newLat,
                            newLng

                    );
                }
            }
        });

        CardView cv_currentLocation = root.findViewById(R.id.cv_currentLocation);
        cv_currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = new LatLng(selfLat, selfLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        return root;
    }

    private void performTambahData(String sNama, String sAlamat, String sJamBuka, String sJamTutup, String encodeImage, String newLat, String newLng) {

        Log.d("img64", "performTambahData: " + encodeImage);
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseModel> call = apiInterface.performTambahCafe(sNama, sAlamat, sJamBuka, sJamTutup, encodeImage, newLat, newLng);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String success = response.body().getSuccess();
//                    Toast.makeText(getApplicationContext(), "RESPON" + response.body().getSuccess(), Toast.LENGTH_SHORT).show();
                    if (success.equals("1")) {
                        Toast toast = Toast.makeText(getContext(), "Berhasil menambah data", Toast.LENGTH_SHORT);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast toast = Toast.makeText(getContext(), "Gagal menambah data. Coba lagi", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                    }
//                    Log.d("success", "onResponse: " + success);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "FAILURE" + t.toString(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getContext(), "Terjadi kesalahan koneksi!", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setPadding(42, 8, 42, 8);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();
            }
        });
    }

    private void pilihFotoFromGallery() {
        Toast toast = Toast.makeText(getContext(), "Disarankan menggunakan gambar mode Landscape", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setPadding(42, 8, 42, 8);
        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
        TextView textView = view.findViewById(android.R.id.message);
        textView.setTextColor(Color.WHITE);
        toast.show();

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        File photoFile = null;
        photoFile = createPhotoFile();

        if (photoFile != null) {
            pathToFile = photoFile.getAbsolutePath();
            Uri photoUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".fileprovider", photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, 3);
        }
    }

    private File createPhotoFile() {
        @SuppressLint("SimpleDateFormat") String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(name, ".jpg", storageDir);
            //File compressedFile = new Compressor(this).compressToFile(image);
        } catch (IOException e) {
//            Log.d("mylog", "Exception : " + e.toString());
        }
        return image;
        //return compressedFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && data != null){
//            Uri selectedImage = data.getData();
//            ImageView imageView = findViewById(R.id.iv_gambarInput);
//            imageView.setImageURI(selectedImage);
//
//            uploadPic();
//
//        }
        if (resultCode == RESULT_OK) {
            if (requestCode == 3) {
                Uri selectedImage = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                ImageView iv_gambarInput = findViewById(R.id.iv_gambarInput);
                iv_fotoTambah.setImageBitmap(bitmap);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inJustDecodeBounds = false;

//                Bitmap bitmapReady = BitmapFactory.decodeFile(bitmap, options);
                Bitmap scaledImage = Bitmap.createScaledBitmap(bitmap, 900, 540, true);
                encodeImage = ImageBase64.encode(scaledImage);
//
//                Log.d("mylog", "encode image: " + encodeImage);
//                Log.d("mylog", "PATH File: " + pathToFile);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);

        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 2);
        }

        getLastLocation();
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    //myLocation = location.toString();
                    selfLat = location.getLatitude();
                    selfLng = location.getLongitude();
                }

                Log.d("lokasi", "Latitude: " + selfLat);
                Log.d("lokasi", "Longitude: " + selfLng);

                setMapMark();
            }
        });
    }

    private void setMapMark() {
        LatLng latLng = new LatLng(selfLat, selfLng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOptions.title("Lokasi kamu");
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.addMarker(markerOptions);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng);

//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOptions.title("Cafe Baru");
        mMap.clear();
        mMap.addMarker(markerOptions);

         newLat = String.valueOf(latLng.latitude);
         newLng = String.valueOf(latLng.longitude);
        save_latTambah.setText(newLat);
        save_lngTambah.setText(newLng);

        Log.d("newpos", "onMapClick: " + latLng.latitude + " || " + latLng.longitude);
    }
}