package com.irfan.pencariancafe.ui.home;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.adapter.AdapterListCafeHome;
import com.irfan.pencariancafe.databinding.FragmentHomeBinding;
import com.irfan.pencariancafe.models.Cafes;
import com.irfan.pencariancafe.utils.ApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeFragment extends Fragment {
    private double[] sortJarak;
    private double selfLat, selfLng;

    List<Cafes> listCafes;
    List<Cafes> listCafesFiltered;
    RecyclerView rv_cafehome;
    RecyclerView.Adapter rvAdapter;
    RecyclerView.LayoutManager rvLayoutManager;
    RequestQueue requestQueue;

    private FragmentHomeBinding binding;

    private FusedLocationProviderClient fusedLocationProviderClient;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        CardView cv_homeLogo = root.findViewById(R.id.cv_homeLogo);
        Animation pulse = AnimationUtils.loadAnimation(getContext(), R.anim.pulse_anim);
        cv_homeLogo.setAnimation(pulse);

        rv_cafehome = root.findViewById(R.id.rv_home);
        rv_cafehome.setVisibility(View.INVISIBLE);

        requestPermission();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        getLastLocation();
        
        requestQueue = Volley.newRequestQueue(getContext());
        listCafes = new ArrayList<>();
        listCafesFiltered = new ArrayList<>();
//        requestAllCafe();
        rvLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_cafehome.setLayoutManager(rvLayoutManager);
        rvAdapter = new AdapterListCafeHome(listCafesFiltered, getContext());
        rv_cafehome.setAdapter(rvAdapter);

        cv_homeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv_homeLogo.setVisibility(View.INVISIBLE);
                rv_cafehome.setVisibility(View.VISIBLE);
                cv_homeLogo.setAnimation(null);
            }
        });

        EditText et_serachHome = root.findViewById(R.id.et_searchHome);
        et_serachHome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0){
                    listCafesFiltered.clear();
                    for (Cafes r : listCafes){
                        if (r.getNama_cafe().toLowerCase().contains(s.toString().toLowerCase())){
                            listCafesFiltered.add(r);
                        }
                    }
                    rvAdapter.notifyDataSetChanged();
                } else {
                    listCafesFiltered.clear();
                    listCafesFiltered.addAll(listCafes);
                    rvAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return root;
    }

    private void requestAllCafe() {
        String apiUrl = ApiClient.API + "getAll_cafeHome.php";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, apiUrl, null,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(JSONArray response) {

                        //get Json
                        Log.d("perform", "cafe home: " + response.toString());
//                        if(response.length() < 1){
//                            tv_alert.setVisibility(View.VISIBLE);
//                        } else {
//                            tv_alert.setVisibility(View.INVISIBLE);
//                        }

//                        sortJarak = new double[response.length()];

//                        Cafes model = new Cafes();
                        //extract datas
                        for(int i=0; i<response.length(); i++){
                            try {
                                JSONObject data = response.getJSONObject(i);
                                //get key 1 by 1
                                Cafes model = new Cafes();

                                model.setId_cafe(data.getString("id_cafe"));
                                model.setNama_cafe(data.getString("nama_cafe"));
                                model.setAlamat(data.getString("alamat"));
                                model.setLat(data.getString("lat"));
                                model.setLng(data.getString("lng"));
                                model.setJam_buka(data.getString("jam_buka"));
                                model.setJam_tutup(data.getString("jam_tutup"));
                                model.setFoto(data.getString("foto"));
                                model.setRating(data.getString("rating"));
                                model.setJarak(getJarak(data.getString("lat"), data.getString("lng")));

                                Log.d("perform", "catchValues HOME ID_CAFE: " + data.getString("id_cafe"));

                                //input model to list
                                listCafesFiltered.add(model);
                                listCafes.add(model);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            rvAdapter.notifyDataSetChanged();
                        }

                        Collections.sort(listCafesFiltered, (a, b)->Double.compare(a.getJarakD(), b.getJarakD()));
                        Collections.sort(listCafes, (a, b)->Double.compare(a.getJarakD(), b.getJarakD()));

                        rvAdapter.notifyDataSetChanged();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("json_respon", "ERROR : " +error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayRequest);
    }

    private String getJarak(String lat, String lng) {
        double radian = 0.0174533;
        double radLat1 = Double.valueOf(selfLat * radian);
        double radLat2 = Double.valueOf(lat) * radian;
        double radLng1 = Double.valueOf(selfLng * radian);
        double radLng2 = Double.valueOf(lng) * radian;

//        =======
        //Delta radian pos 1 - pos 2
        BigDecimal delRadLat = BigDecimal.valueOf(radLat2).subtract(BigDecimal.valueOf(radLat1));
        BigDecimal delRadLng = BigDecimal.valueOf(radLng2).subtract(BigDecimal.valueOf(radLng1));

//        =======
//        Val a
        double valA = Math.sin(Math.pow((delRadLat.doubleValue() / 2), 2));
//        Val c
        double valC = Math.sin(Math.pow((delRadLng.doubleValue() / 2), 2));

//        =====
//        Val D
        double valD = 2 * 6371 * Math.asin(Math.sqrt(valA + valC));

//        =====
        String jarak = String.valueOf(valD);
//        Log.d("posisi", "jarak: " + jarak);


//        double gotJarak = 0.0;
        return String.valueOf(jarak);
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null){
                    //myLocation = location.toString();
                    selfLat = location.getLatitude();
                    selfLng = location.getLongitude();
                }

//                Log.d("lokasi", "Latitude: " + selfLat);
//                Log.d("lokasi", "Longitude: " + selfLng);

//                setMapMark();
                requestAllCafe();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}