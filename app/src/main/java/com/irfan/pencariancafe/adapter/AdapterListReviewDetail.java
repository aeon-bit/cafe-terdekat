package com.irfan.pencariancafe.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.models.Cafes;

import java.util.List;

public class AdapterListReviewDetail extends
        RecyclerView.Adapter<AdapterListReviewDetail.HolderItem> {

    List<Cafes> listReviews;
    Context context;

    public AdapterListReviewDetail(List<Cafes> listReviews, Context context) {
        this.listReviews = listReviews;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        //initiate item layout
        View view = LayoutInflater.from(context)
                .inflate(R.layout.itemlist_review_detail, viewGroup, false);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HolderItem holderItem, int position) {
        //memasukan data ke objek yg sudah dikenalkan di HolderItem
        final Cafes posCafes = listReviews.get(position);

        holderItem.tv_namalistDetail.setText(posCafes.getNama_user());
        holderItem.tv_reviewlistDetail.setText(posCafes.getReview());

        //load image
//        Glide.with(context).load(ApiClient.IMAGE_URL + listReviews.get(position)
//                .getFoto()).placeholder(R.drawable.nopic).centerCrop().into(holderItem.icv_fotoListHome);

    }

    @Override
    public int getItemCount() {
        return listReviews.size();
    }


    //initiate item layout
    public class HolderItem extends RecyclerView.ViewHolder {
        TextView tv_namalistDetail, tv_reviewlistDetail;

        CardView cv_itemListReviewDetail;

        public HolderItem(View v) {
            super(v);

            tv_namalistDetail = v.findViewById(R.id.tv_namalistDetail);
            tv_reviewlistDetail = v.findViewById(R.id.tv_reviewListDetail);

            cv_itemListReviewDetail = v.findViewById(R.id.cv_itemListReviewDetail);
        }
    }
}
