package com.irfan.pencariancafe.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.irfan.pencariancafe.Detail;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.models.Cafes;
import com.irfan.pencariancafe.utils.ApiClient;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterListCafeHome extends
        RecyclerView.Adapter<AdapterListCafeHome.HolderItem> {

    List<Cafes> listCafes;
    Context context;

    public AdapterListCafeHome(List<Cafes> listCafes, Context context) {
        this.listCafes = listCafes;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        //initiate item layout
        View view = LayoutInflater.from(context)
                .inflate(R.layout.itemlist_cafe_home, viewGroup, false);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull HolderItem holderItem, int position) {
        //memasukan data ke objek yg sudah dikenalkan di HolderItem
        final Cafes posCafes = listCafes.get(position);

        holderItem.tv_namalistHome.setText(posCafes.getNama_cafe());
        holderItem.tv_alamatlistHome.setText(posCafes.getAlamat());
        holderItem.tv_jaraklistHome.setText(posCafes.getJarak().substring(0, 4) + " km");
        if (posCafes.getRating().equals("1")){
            holderItem.iv_ratingListHome.setImageResource(R.drawable.rating1);
        } else if (posCafes.getRating().equals("2")){
            holderItem.iv_ratingListHome.setImageResource(R.drawable.rating2);
        } else if (posCafes.getRating().equals("3")){
            holderItem.iv_ratingListHome.setImageResource(R.drawable.rating3);
        } else if (posCafes.getRating().equals("4")){
            holderItem.iv_ratingListHome.setImageResource(R.drawable.rating4);
        } else if (posCafes.getRating().equals("5")){
            holderItem.iv_ratingListHome.setImageResource(R.drawable.rating5);
        }

        //load image
        Glide.with(context).load(ApiClient.IMAGE_URL + listCafes.get(position)
                .getFoto()).placeholder(R.drawable.nopic).centerCrop().into(holderItem.icv_fotoListHome);


        holderItem.cv_itemListCafeHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Detail.class);

                i.putExtra("id_cafe", posCafes.getId_cafe());
                i.putExtra("nama_cafe", posCafes.getNama_cafe());
                i.putExtra("alamat", posCafes.getAlamat());
                i.putExtra("lat", posCafes.getLat());
                i.putExtra("lng", posCafes.getLng());
                i.putExtra("jam_buka", posCafes.getJam_buka());
                i.putExtra("jam_tutup", posCafes.getJam_tutup());
                i.putExtra("foto", posCafes.getFoto());
                i.putExtra("jarak", posCafes.getJarak());

                Log.d("perform", "catchValues ADAPT ID: " + posCafes.getId_cafe());

                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(i);

//                AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
//                DetailFragment detailFragment = new DetailFragment();
//                appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_activity_main, detailFragment)
//                        .addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCafes.size();
    }


    //initiate item layout
    public class HolderItem extends RecyclerView.ViewHolder {
        TextView tv_namalistHome, tv_alamatlistHome, tv_jaraklistHome;
        CircleImageView icv_fotoListHome;
        ImageView iv_ratingListHome;

        CardView cv_itemListCafeHome;

        public HolderItem(View v) {
            super(v);

            tv_namalistHome = v.findViewById(R.id.tv_namalistHome);
            tv_alamatlistHome = v.findViewById(R.id.tv_alamatListHome);
            tv_jaraklistHome = v.findViewById(R.id.tv_jarakListHome);

            icv_fotoListHome = v.findViewById(R.id.icv_fotoListHome);
            iv_ratingListHome = v.findViewById(R.id.iv_ratingListHome);

            cv_itemListCafeHome = v.findViewById(R.id.cv_itemListCafeHome);
        }
    }
}
