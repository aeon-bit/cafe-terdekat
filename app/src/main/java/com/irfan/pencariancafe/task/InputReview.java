package com.irfan.pencariancafe.task;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.irfan.pencariancafe.Detail;
import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.auth.LoginFragment;
import com.irfan.pencariancafe.models.Reviews;
import com.irfan.pencariancafe.models.Users;
import com.irfan.pencariancafe.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputReview extends Dialog implements View.OnClickListener{

    ImageView iv_inputRating;
    private int rating = 0;

    public InputReview(@NonNull Context context) {
        super(context);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);

        setTitle(null);
        setCancelable(true);
        setOnCancelListener(null);

        View view = LayoutInflater.from(context).inflate(R.layout.input_review_layout, null);
        setContentView(view);

        iv_inputRating = view.findViewById(R.id.iv_inputRating);

        LinearLayout ly_r1 = view.findViewById(R.id.ly_r1);
        LinearLayout ly_r2 = view.findViewById(R.id.ly_r2);
        LinearLayout ly_r3 = view.findViewById(R.id.ly_r3);
        LinearLayout ly_r4 = view.findViewById(R.id.ly_r4);
        LinearLayout ly_r5 = view.findViewById(R.id.ly_r5);
        ly_r1.setOnClickListener(this::onClick);
        ly_r2.setOnClickListener(this::onClick);
        ly_r3.setOnClickListener(this::onClick);
        ly_r4.setOnClickListener(this::onClick);
        ly_r5.setOnClickListener(this::onClick);

        TextView save_id = view.findViewById(R.id.save_id);
        EditText et_isiReview = view.findViewById(R.id.et_isiReview);
        CardView cv_btnSimpanReview = view.findViewById(R.id.cv_btnSimpanReview);
        cv_btnSimpanReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.isLogin()) {
                    if (et_isiReview.getText().toString().equals("")) {
                        et_isiReview.setError("Masukkan ulasan");
                    } else if (rating == 0) {
                        et_isiReview.setError("Masukkan bintang rating");
                    } else {
                        simpanUlasan(save_id.getText().toString(), et_isiReview.getText().toString());
                    }
                } else {
                    Toast toast = Toast.makeText(getContext(), "Silakan login dahulu", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();
                }
            }
        });

    }

    private void simpanUlasan(String id_cafe, String review) {
//        Log.d("saverev", "ID: " + id_cafe);
//        Log.d("saverev", "REVIEW: " + review);
//        Log.d("saverev", "ID USER: " + SessionManager.getIdUser());
//        Log.d("saverev", "RATING: " + rating);
        Call<Reviews> call = LoginRegisterActivity.apiInterface.performSimpanReview(id_cafe, review, SessionManager.getIdUser(), String.valueOf(rating));
        call.enqueue(new Callback<Reviews>() {
            @Override
            public void onResponse(Call<Reviews> call, Response<Reviews> response) {

                if (response.body().getResponse().equals("ok")){

                    Toast toast = Toast.makeText(getContext(), "Berhasil menambah ulasan", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();

                    dismiss();

                }else {
//                        LoginRegisterActivity.prefConfig.displayToast("Something went wrong...");
//                        Log.d("msg", "error");
                    Toast toast = Toast.makeText(getContext(), "Gagal! Coba lagi", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Reviews> call, Throwable t) {

//                    Log.d("msg", "onFaillure");
                Toast toast = Toast.makeText(getContext(), "Kesalahan koneksi", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ly_r1:
                iv_inputRating.setImageResource(R.drawable.ic_inrate_1);
                rating = 1;
                break;
            case R.id.ly_r2:
                iv_inputRating.setImageResource(R.drawable.ic_inrate_2);
                rating = 2;
                break;
            case R.id.ly_r3:
                iv_inputRating.setImageResource(R.drawable.ic_inrate_3);
                rating = 3;
                break;
            case R.id.ly_r4:
                iv_inputRating.setImageResource(R.drawable.ic_inrate_4);
                rating = 4;
                break;
            case R.id.ly_r5:
                iv_inputRating.setImageResource(R.drawable.ic_inrate_5);
                rating = 5;
                break;
            default:
                iv_inputRating.setImageResource(R.drawable.ic_input_rating);
                rating = 0;
        }
    }
}
