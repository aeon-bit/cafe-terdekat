package com.irfan.pencariancafe.task;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.models.Reviews;
import com.irfan.pencariancafe.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteDialog extends Dialog{

    ImageView iv_inputRating;
    private int rating = 0;

    public DeleteDialog(@NonNull Context context) {
        super(context);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);

        setTitle(null);
        setCancelable(true);
        setOnCancelListener(null);

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_delete_layout, null);
        setContentView(view);

    }
}
