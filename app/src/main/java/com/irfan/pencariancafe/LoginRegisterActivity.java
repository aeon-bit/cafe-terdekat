package com.irfan.pencariancafe;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.irfan.pencariancafe.auth.LoginFragment;
import com.irfan.pencariancafe.auth.RegistFragment;
import com.irfan.pencariancafe.auth.WelcomeFragment;
import com.irfan.pencariancafe.utils.ApiClient;
import com.irfan.pencariancafe.utils.ApiInterface;
import com.irfan.pencariancafe.utils.PrefConfig;
import com.irfan.pencariancafe.utils.SessionManager;


public class LoginRegisterActivity extends AppCompatActivity implements LoginFragment.OnLoginFormActivityListener, WelcomeFragment.OnLogoutListener {

    public static PrefConfig prefConfig;
    public static ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        if (Build.VERSION.SDK_INT > 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }


//        prefConfig = new PrefConfig(this);
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        if (findViewById(R.id.fl_fragmentContainer) != null){

                if (savedInstanceState != null){
                    return;
                }

            /*if (prefConfig.readLoginStatus()){
                getSupportFragmentManager().beginTransaction().add(R.id.fl_fragmentContainer, new WelcomeFragment()).commit();
            }else{
                getSupportFragmentManager().beginTransaction().add(R.id.fl_fragmentContainer, new LoginFragment()).commit();
            }*/
        }

        //check login for fragment show
        if (SessionManager.isLogin()){
//            getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer, new WelcomeFragment()).commit();
            Intent intent = new Intent(LoginRegisterActivity.this, MainActivity.class);
            startActivity(intent);
//            overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer, new LoginFragment()).commit();
        }
    }

    @Override
    public void performRegister() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer,
                new RegistFragment()).addToBackStack(null).commit();
    }

    @Override
    public void performLogin() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer,
                new LoginFragment()).addToBackStack(null).commit();
    }

//    @Override
//    public void performLogin(String Name) {
//        //prefConfig.writeName(Name);
//        //getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer, new WelcomeFragment()).commit();
//        Intent intent = new Intent(this, LoginRegisterActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TOP
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
//
//    }

    @Override
    public void logoutPerform() {
//        prefConfig.writeLoginStatus(false);
//        prefConfig.writeName("Users");
//        getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer, new LoginFragment()).commit();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        Intent i;
        i = new Intent(LoginRegisterActivity.this, MainActivity.class);
        startActivity(i);
    }
}
