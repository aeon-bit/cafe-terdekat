package com.irfan.pencariancafe;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.irfan.pencariancafe.adapter.AdapterListReviewDetail;
import com.irfan.pencariancafe.models.Cafes;
import com.irfan.pencariancafe.models.ResponseModel;
import com.irfan.pencariancafe.task.DeleteDialog;
import com.irfan.pencariancafe.task.InputReview;
import com.irfan.pencariancafe.utils.ApiClient;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.irfan.pencariancafe.utils.ApiInterface;
import com.irfan.pencariancafe.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detail extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double lat, lng;
    private String id_cafe, nama_cafe, alamat, jam_buka, jam_tutup, foto;
    private float avgRating;

    private RequestQueue requestQueue;

    List<Cafes> listReviews;
    RecyclerView rv_reviewDetail;
    RecyclerView.Adapter rvAdapter;
    RecyclerView.LayoutManager rvLayoutManager;
    RequestQueue requestQueueReview;

    TextView tv_totalRatingDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        requestQueue = Volley.newRequestQueue(this);

        rv_reviewDetail = findViewById(R.id.rv_reviewDetail);
        requestQueueReview = Volley.newRequestQueue(this);
        listReviews = new ArrayList<>();

        rvLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_reviewDetail.setLayoutManager(rvLayoutManager);
        rvAdapter = new AdapterListReviewDetail(listReviews, this);
        rv_reviewDetail.setAdapter(rvAdapter);

        catchValues();

        ImageView iv_btnArrowBack = findViewById(R.id.iv_btnArrowBack);
        iv_btnArrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Detail.this, MainActivity.class);
                startActivity(i);
            }
        });

        ImageView iv_btnInputRating = findViewById(R.id.iv_btnInputRating);
        iv_btnInputRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputReview inputReview = new InputReview(Detail.this);
                inputReview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                TextView save_id = inputReview.findViewById(R.id.save_id);
                save_id.setText(id_cafe);
                inputReview.show();
            }
        });

        CardView cv_btnUbah = findViewById(R.id.cv_btnUbah);
        cv_btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Detail.this, Ubah.class);
                intent.putExtra("id_cafe", id_cafe);
                intent.putExtra("nama_cafe", nama_cafe);
                intent.putExtra("alamat", alamat);
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("jam_buka", jam_buka);
                intent.putExtra("jam_tutup", jam_tutup);
                intent.putExtra("foto", foto);

                startActivity(intent);
            }
        });

        CardView cv_btnDelete = findViewById(R.id.cv_btnDelete);
        cv_btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusCafe();
            }
        });

        cv_btnUbah.setVisibility(View.INVISIBLE);
        cv_btnDelete.setVisibility(View.INVISIBLE);
        if (SessionManager.isLogin() && SessionManager.getRole().equals("admin")) {
            cv_btnUbah.setVisibility(View.VISIBLE);
            cv_btnDelete.setVisibility(View.VISIBLE);
        }
    }

    private void hapusCafe() {
        DeleteDialog deleteDialog = new DeleteDialog(Detail.this);
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CardView cv_btnConfirmHapus = deleteDialog.findViewById(R.id.cv_btnConfirmHapus);
        CardView cv_btnBatalHapus = deleteDialog.findViewById(R.id.cv_btnBatalHapus);
        cv_btnConfirmHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performDelCafe();
            }
        });

        cv_btnBatalHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void performDelCafe() {
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseModel> call = apiInterface.performHapusCafe(id_cafe);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String success = response.body().getSuccess();
//                    Toast.makeText(getApplicationContext(), "RESPON" + response.body().getSuccess(), Toast.LENGTH_SHORT).show();
                    if (success.equals("1")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Berhasil menghapus data", Toast.LENGTH_SHORT);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Gagal menghapus data. Coba lagi", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                    }
//                    Log.d("success", "onResponse: " + success);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "FAILURE" + t.toString(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(), "Terjadi kesalahan koneksi!", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setPadding(42, 8, 42, 8);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();
            }
        });
    }

    private void catchValues() {
        id_cafe = getIntent().getExtras().getString("id_cafe");
        nama_cafe = getIntent().getExtras().getString("nama_cafe");
        alamat = getIntent().getExtras().getString("alamat");
        lat = Double.parseDouble(getIntent().getExtras().getString("lat"));
        lng = Double.parseDouble(getIntent().getExtras().getString("lng"));
        jam_buka = getIntent().getExtras().getString("jam_buka");
        jam_tutup = getIntent().getExtras().getString("jam_tutup");
        foto = getIntent().getExtras().getString("foto");

//        Log.d("perform", "catchValuesDetail ID: " + id_cafe);

        TextView tv_namaDetail = findViewById(R.id.tv_namaDetail);
        TextView tv_jamDetail = findViewById(R.id.tv_jamDetail);
        TextView tv_alamatDetail = findViewById(R.id.tv_alamatDetail);
        tv_totalRatingDetail = findViewById(R.id.tv_totalRatingDetail);
        ImageView iv_fotoDetail = findViewById(R.id.iv_fotoDetail);

        tv_alamatDetail.setText(alamat);
        tv_namaDetail.setText(nama_cafe);
        tv_jamDetail.setText(jam_buka.substring(0, 5) + " - " + jam_tutup.substring(0, 5));
        Glide.with(this).load(ApiClient.IMAGE_URL + foto)
                .error(R.drawable.nopic).into(iv_fotoDetail);

        initMap();
        getRating(id_cafe);
        getReviews(id_cafe);
    }

    private void getReviews(String id_cafe) {
        String apiUrl = ApiClient.API + "getAll_reviewById.php?id_cafe=" + id_cafe;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, apiUrl, null,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(JSONArray response) {

                        //get Json
                        Log.d("respon", "resto: " + response.toString());
                        //extract datas
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject data = response.getJSONObject(i);
                                //get key 1 by 1
                                Cafes model = new Cafes();

                                model.setNama_user(data.getString("nama_user"));
                                model.setReview(data.getString("review"));

                                //input model to list
                                listReviews.add(model);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            rvAdapter.notifyDataSetChanged();
                        }

                        rvAdapter.notifyDataSetChanged();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("json_respon", "ERROR : " + error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayRequest);
    }

    private void getRating(String id_cafe) {
        String apiUrl = ApiClient.API + "getAll_ratingById.php?id_cafe=" + id_cafe;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, apiUrl, null,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(JSONArray response) {

                        //get Json
                        Log.d("respon", "resto: " + response.toString());

                        int total = 0;
                        String count = String.valueOf(response.length());
                        //extract datas
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject data = response.getJSONObject(i);
                                //get key 1 by 1
                                Cafes model = new Cafes();

//                                model.setRating(data.getString("rating"));
                                total = total + Integer.valueOf(data.getString("rating"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (response.length() > 0) {

                            avgRating = total / response.length();
                            showrating(avgRating);
                            tv_totalRatingDetail.setText(count + " Rating");
                        } else {
                            tv_totalRatingDetail.setText(0 + " Rating");
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("json_respon", "ERROR : " + error.getMessage());
                    }
                });

        requestQueue.add(jsonArrayRequest);
    }

    private void showrating(float avgRating) {
        ImageView iv_ratingDetail = findViewById(R.id.iv_ratingDetail);
        if (avgRating <= 1) {
            iv_ratingDetail.setImageResource(R.drawable.rating1);
        } else if (avgRating <= 2) {
            iv_ratingDetail.setImageResource(R.drawable.rating2);
        } else if (avgRating <= 3) {
            iv_ratingDetail.setImageResource(R.drawable.rating3);
        } else if (avgRating <= 4) {
            iv_ratingDetail.setImageResource(R.drawable.rating4);
        } else {
            iv_ratingDetail.setImageResource(R.drawable.rating5);
        }
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_detail);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng pos = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(pos).title(nama_cafe));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16.0f));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(pos));
    }
}