package com.irfan.pencariancafe;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.os.Environment.getExternalStoragePublicDirectory;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.irfan.pencariancafe.models.ResponseModel;
import com.irfan.pencariancafe.utils.ApiClient;
import com.irfan.pencariancafe.utils.ApiInterface;
import com.kosalgeek.android.photoutil.ImageBase64;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ubah extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {
    private GoogleMap mMap;

    private String id_cafe, nama_cafe, foto;
    private double lat, lng;

    String pathToFile, encodeImage = "", newLat, newLng;

    EditText save_latUbah, save_lngUbah;
    EditText et_namaUbah, et_alamatUbah;
    ImageView iv_fotoUbah;

    Spinner sp_jamBuka, sp_jamTutup;
    int ubahFoto = 0;

    private FusedLocationProviderClient fusedLocationProviderClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah);

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        //init map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_Ubah);
        mapFragment.getMapAsync(this);
        requestPermission();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        //spinner jam
         sp_jamBuka = findViewById(R.id.sp_jamBuka);
         sp_jamTutup = findViewById(R.id.sp_jamTutup);
        ArrayAdapter<CharSequence> adapterJam = ArrayAdapter.createFromResource(this, R.array.jam, android.R.layout.simple_spinner_item);
        adapterJam.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp_jamBuka.setAdapter(adapterJam);
        sp_jamTutup.setAdapter(adapterJam);

        save_latUbah = findViewById(R.id.save_latUbah);
        save_lngUbah = findViewById(R.id.save_lngUbah);

        catchValues();

        iv_fotoUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihFotoFromGallery();
            }
        });

        CardView cv_btnPerformUbahData = findViewById(R.id.cv_btnPerformUbahData);
        cv_btnPerformUbahData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nowFoto;
                if (encodeImage.equals("")){
                    nowFoto = foto;
                    Log.d("pressed", "NOT UBAH: " + nowFoto);
                } else {
                    nowFoto = encodeImage;
                    ubahFoto = 1;
                    Log.d("pressed", "UBAH: " + nowFoto);
                }
                if (et_namaUbah.getText().toString().equals("")){
                    et_namaUbah.setError("Masukkan nama");
                } else if (et_alamatUbah.getText().toString().equals("")){
                    et_alamatUbah.setError("Masukkan alamat");
                } else if (save_latUbah.getText().toString().equals("")){
                    save_latUbah.setError("Masukkan lokasi");
                } else if (save_lngUbah.getText().toString().equals("")){
                    save_lngUbah.setError("Masukkan lokasi");
                } else {
                    performUbahData(
                            et_namaUbah.getText().toString(),
                            et_alamatUbah.getText().toString(),
                            sp_jamBuka.getSelectedItem().toString(),
                            sp_jamTutup.getSelectedItem().toString(),
                            nowFoto,
                            save_latUbah.getText().toString(),
                            save_lngUbah.getText().toString(),
                            String.valueOf(ubahFoto)

                    );
                }
            }
        });
    }

    private void catchValues() {
        id_cafe = getIntent().getExtras().getString("id_cafe", "0");
         nama_cafe = getIntent().getExtras().getString("nama_cafe", "0");
        String alamat = getIntent().getExtras().getString("alamat", "0");
        lat = getIntent().getExtras().getDouble("lat");
        lng = getIntent().getExtras().getDouble("lng");
        String jam_buka = getIntent().getExtras().getString("jam_buka", "0");
        String jam_tutup = getIntent().getExtras().getString("jam_tutup", "0");
        foto = getIntent().getExtras().getString("foto", "0");

//        Log.d("perform", "catchValues ID: " + id_cafe);

         et_namaUbah = findViewById(R.id.et_namaUbah);
         et_alamatUbah = findViewById(R.id.et_alamatUbah);
         iv_fotoUbah = findViewById(R.id.iv_fotoUbah);

        et_namaUbah.setText(nama_cafe);
        et_alamatUbah.setText(alamat);
        sp_jamBuka.setSelection(convertJam(jam_buka));
        sp_jamTutup.setSelection(convertJam(jam_tutup));

        String sLat = String.valueOf(lat);
        String sLng = String.valueOf(lng);
        save_latUbah.setText(sLat);
        save_lngUbah.setText(sLng);

        Glide.with(this).load(ApiClient.IMAGE_URL + foto).error(R.drawable.nopic)
                .into(iv_fotoUbah);

    }

    public int convertJam(String jam) {
        int newJam = Integer.parseInt(jam.substring(0, 2));

        Log.d("convert", "Jam: " + jam);
        Log.d("convert", "covertJam: " + newJam);

        return newJam;
    }

    private void performUbahData(String sNama, String sAlamat, String sJamBuka, String sJamTutup, String sFoto, String newLat, String newLng, String ubahFoto) {


        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseModel> call = apiInterface.performUbahCafe(id_cafe, sNama, sAlamat, sJamBuka, sJamTutup, sFoto, newLat, newLng, ubahFoto);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String success = response.body().getSuccess();
//                    Toast.makeText(getApplicationContext(), "RESPON" + response.body().getSuccess(), Toast.LENGTH_SHORT).show();
                    if (success.equals("1")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Berhasil mengubah data", Toast.LENGTH_SHORT);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Gagal mengubah data. Coba lagi", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setPadding(42, 8, 42, 8);
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                    }
//                    Log.d("success", "onResponse: " + success);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "FAILURE" + t.toString(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(), "Terjadi kesalahan koneksi!", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setPadding(42, 8, 42, 8);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();
            }
        });
    }

    private void pilihFotoFromGallery() {
        Toast toast = Toast.makeText(this, "Disarankan menggunakan gambar mode Landscape", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setPadding(42, 8, 42, 8);
        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
        TextView textView = view.findViewById(android.R.id.message);
        textView.setTextColor(Color.WHITE);
        toast.show();

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        File photoFile = null;
        photoFile = createPhotoFile();

        if (photoFile != null) {
            pathToFile = photoFile.getAbsolutePath();
            Uri photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", photoFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, 3);
        }
    }

    private File createPhotoFile() {
        @SuppressLint("SimpleDateFormat") String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(name, ".jpg", storageDir);
            //File compressedFile = new Compressor(this).compressToFile(image);
        } catch (IOException e) {
//            Log.d("mylog", "Exception : " + e.toString());
        }
        return image;
        //return compressedFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && data != null){
//            Uri selectedImage = data.getData();
//            ImageView imageView = findViewById(R.id.iv_gambarInput);
//            imageView.setImageURI(selectedImage);
//
//            uploadPic();
//
//        }
        if (resultCode == RESULT_OK) {
            if (requestCode == 3) {
                Uri selectedImage = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                ImageView iv_gambarInput = findViewById(R.id.iv_gambarInput);
                iv_fotoUbah.setImageBitmap(bitmap);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inJustDecodeBounds = false;

//                Bitmap bitmapReady = BitmapFactory.decodeFile(bitmap, options);
                Bitmap scaledImage = Bitmap.createScaledBitmap(bitmap, 900, 540, true);
                encodeImage = ImageBase64.encode(scaledImage);
//
//                Log.d("mylog", "encode image: " + encodeImage);
//                Log.d("mylog", "PATH File: " + pathToFile);
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng);

//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOptions.title("Cafe Baru");
        mMap.clear();
        mMap.addMarker(markerOptions);

        newLat = String.valueOf(latLng.latitude);
        newLng = String.valueOf(latLng.longitude);
        save_latUbah.setText(newLat);
        save_lngUbah.setText(newLng);
//        Log.d("newPos", "LAT: " + latLng.latitude);
//        Log.d("newPos", "LNG: " + latLng.longitude);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);

        setMapMark();
    }

    private void setMapMark() {
        LatLng latLng = new LatLng(lat, lng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOptions.title(nama_cafe);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.addMarker(markerOptions);
    }
}