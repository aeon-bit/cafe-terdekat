package com.irfan.pencariancafe.auth;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.models.Users;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistFragment extends Fragment {

    LoginFragment.OnLoginFormActivityListener loginFormActivityListener;

    public interface OnLoginFormActivityListener {
        public void performRegister();

        public void performLogin();
    }

    public RegistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_regist, container, false);

        EditText et_daftarNama = view.findViewById(R.id.et_daftarNama);
        EditText et_daftarUsername = view.findViewById(R.id.et_daftarUsername);
        EditText et_daftarPass = view.findViewById(R.id.et_daftarPass);
        EditText et_daftarCPass = view.findViewById(R.id.et_daftarCPass);
//        CircleImageView icv_daftarFoto = view.findViewById(R.id.icv_daftarFoto);

        CardView cv_btnPerformDaftar = view.findViewById(R.id.cv_btnPerformDaftar);
        TextView tv_btnLoginNow = view.findViewById(R.id.tv_btnLoginNow);

        tv_btnLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginRegisterActivity)getActivity()).performLogin();
            }
        });

        cv_btnPerformDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if (et_daftarNama.getText().toString().equals("")){
                    et_daftarNama.setError("Masukkan nama");
                } else if (et_daftarUsername.getText().toString().equals("")){
                    et_daftarUsername.setError("Masukkan Username");
                } else if (et_daftarPass.getText().toString().equals("")){
                    et_daftarPass.setError("Masukkan Password");
                } else if (et_daftarCPass.getText().toString().equals("")) {
                    et_daftarCPass.setError("Konfirmasi password");
                } else if (!et_daftarCPass.getText().toString().equals(et_daftarPass.getText().toString())){
                    et_daftarCPass.setError("Password tidak sama");
                } else {
                    
                    performRegistration(
                            et_daftarNama.getText().toString(),
                            et_daftarUsername.getText().toString(),
                            et_daftarPass.getText().toString()
                            
                    );
                }
            }
        });

        return view;
    }

    public void performRegistration(String nama, String username, String pass){
        
            Call<Users> call = LoginRegisterActivity.apiInterface.performRegistration(nama, username, pass);
            call.enqueue(new Callback<Users>() {
                @Override
                public void onResponse(Call<Users> call, Response<Users> response) {

                    if (response.body().getResponse().equals("ok")){


                        Toast toast = Toast.makeText(getActivity(), "Pendaftaran berhasil", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();

                        Fragment fragment = new LoginFragment();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragmentContainer, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                    }else if (response.body().getResponse().equals("exist")){
//                        LoginRegisterActivity.prefConfig.displayToast("Users already Exist...");
//                        Log.d("msg", "ex");
                        Toast toast = Toast.makeText(getActivity(), "Username terpakai", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();
                    }else if (response.body().getResponse().equals("error")){
//                        LoginRegisterActivity.prefConfig.displayToast("Something went wrong...");
//                        Log.d("msg", "error");
                        Toast toast = Toast.makeText(getActivity(), "Gagal! Coba lagi", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                        TextView textView = view.findViewById(android.R.id.message);
                        textView.setTextColor(Color.WHITE);
                        toast.show();
                    }
                }

                @Override
                public void onFailure(Call< Users > call, Throwable t) {

//                    Log.d("msg", "onFaillure");
                    Toast toast = Toast.makeText(getActivity(), "Kesalahan koneksi", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();
                }
            });
        }

}
