package com.irfan.pencariancafe.auth;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.MainActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.models.Users;
import com.irfan.pencariancafe.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private TextView tvBtn_registerNow;
    private EditText et_loginUsername, et_loginPass;
    private CardView cv_btnPerformLogin;

    RelativeLayout rl_progressBarLogin;

    OnLoginFormActivityListener loginFormActivityListener;

    public interface OnLoginFormActivityListener {
        public void performRegister();

        public void performLogin();
    }


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        tvBtn_registerNow = view.findViewById(R.id.tvBtn_registerNow);
        rl_progressBarLogin = view.findViewById(R.id.rl_progressBarLogin);

        et_loginUsername = view.findViewById(R.id.et_loginUsername);
        et_loginPass = view.findViewById(R.id.et_loginPass);
        cv_btnPerformLogin = view.findViewById(R.id.cv_btnPerformLogin);

        rl_progressBarLogin.setVisibility(View.INVISIBLE);

        cv_btnPerformLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLogin();
            }
        });

        tvBtn_registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFormActivityListener.performRegister();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        loginFormActivityListener = (OnLoginFormActivityListener) activity;
    }

    private void performLogin() {
        rl_progressBarLogin.setVisibility(View.VISIBLE);
        String sUsername = et_loginUsername.getText().toString().trim();
        String sPassword = et_loginPass.getText().toString();

        Call<Users> call = LoginRegisterActivity.apiInterface.performUserLogin(sUsername, sPassword);
        call.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                rl_progressBarLogin.setVisibility(View.INVISIBLE);
                if (response.body().getResponse().equals("ok")) {
                    SessionManager.login(response.body());
//                    Log.d("login", "onResponse: " + response.body());
                    //SessionManager.;
                    Toast toast = Toast.makeText(getActivity(), "Login Berhasil", Toast.LENGTH_SHORT);
                    View view = toast.getView();
                    view.setPadding(42, 8, 42, 8);
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);

//                    LoginRegisterActivity.prefConfig.writeLoginStatus(true);
//                    loginFormActivityListener.performLogin(response.body().getEmail());
                } else if (response.body().getResponse().equals("failed")) {
                    rl_progressBarLogin.setVisibility(View.INVISIBLE);
//                    LoginRegisterActivity.prefConfig.displayToast("");
                    Toast toast = Toast.makeText(getActivity(), "Username/Password Salah!", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setPadding(42, 8, 42, 8);
                    view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                    TextView textView = view.findViewById(android.R.id.message);
                    textView.setTextColor(Color.WHITE);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Log.d("login", "onFailure: " + t);
                rl_progressBarLogin.setVisibility(View.INVISIBLE);
                Toast toast = Toast.makeText(getActivity(), "Terjadi Kesalahan!, Cek koneksi internet!", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setPadding(42, 8, 42, 8);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_red);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();
            }
        });

//        et_emailLogin.setText("");
//        et_passwordLogin.setText("");
    }
}

