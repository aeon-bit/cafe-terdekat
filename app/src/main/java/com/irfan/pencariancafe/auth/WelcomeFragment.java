package com.irfan.pencariancafe.auth;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.irfan.pencariancafe.LoginRegisterActivity;
import com.irfan.pencariancafe.R;
import com.irfan.pencariancafe.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {
    private TextView tv_welcomeName;
    private TextView btn_logout, btn_logoutHome;
    OnLogoutListener logoutListener;

    public interface OnLogoutListener{
        public void logoutPerform();
    }


    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        tv_welcomeName = view.findViewById(R.id.tv_welcomeName);
        btn_logout = view.findViewById(R.id.btn_logoutWelcome);
        btn_logoutHome = view.findViewById(R.id.btn_logoutHome);

        tv_welcomeName.setText(SessionManager.getUsername());
//        Log.d("login", "NAMA: " + SessionManager.getNamaPengguna());
//        Log.d("login", "USER: " + SessionManager.getEmail());
//        Log.d("login", "PASS: " + SessionManager.getPass());

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.logout();

                Toast toast = Toast.makeText(getActivity(), "Logout berhasil", Toast.LENGTH_LONG);
                View view = toast.getView();
                view.setPadding(42,16,42,16);
                view.setBackgroundResource(R.drawable.xmlbg_fill_btn_ok);
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(Color.WHITE);
                toast.show();

                logoutListener.logoutPerform();

                Intent intent = new Intent(getContext(), LoginRegisterActivity.class);
                startActivity(intent);

            }
        });

        btn_logoutHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
//                if (SessionManager.getHakAkses().equals("Pimpinan")) {
//                    intent = new Intent(getContext(), MainMenuPimpinan.class);
//                    startActivity(intent);
//                } else if (SessionManager.getHakAkses().equals("Gudang")){
//                    intent = new Intent(getContext(), MainMenuGudang.class);
//                    startActivity(intent);
//                } else if (SessionManager.getHakAkses().equals("Kasir")){
//                    intent = new Intent(getContext(), MainMenuKasir.class);
//                    startActivity(intent);
//                } else {
//                    intent = new Intent(getContext(), MainMenuSalesCounter.class);
//                    startActivity(intent);
//                }

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        logoutListener = (OnLogoutListener) activity;
    }
}
