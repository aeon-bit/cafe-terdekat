package com.irfan.pencariancafe.utils;

import com.irfan.pencariancafe.models.ResponseModel;
import com.irfan.pencariancafe.models.Reviews;
import com.irfan.pencariancafe.models.Users;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("login.php")
    Call<Users> performUserLogin(
            @Query("username") String Username,
            @Query("password") String Password
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<Users> performRegistration(
            @Field("nama_user") String nama_user,
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("review.php")
    Call<Reviews> performSimpanReview(
            @Field("id_cafe") String id_cafe,
            @Field("review") String review,
            @Field("id_user") String id_user,
            @Field("rating") String rating
    );


    @FormUrlEncoded
    @POST("tambahCafe.php")
    Call<ResponseModel> performTambahCafe(
            @Field("nama_cafe") String nama_cafe,
            @Field("alamat") String alamat,
            @Field("jam_buka") String jam_buka,
            @Field("jam_tutup") String jam_tutup,
            @Field("foto") String foto,
            @Field("lat") String lat,
            @Field("lng") String lng
    );

    @FormUrlEncoded
    @POST("ubahCafe.php")
    Call<ResponseModel> performUbahCafe(
            @Field("id_cafe") String id_cafe,
            @Field("nama_cafe") String nama_cafe,
            @Field("alamat") String alamat,
            @Field("jam_buka") String jam_buka,
            @Field("jam_tutup") String jam_tutup,
            @Field("foto") String foto,
            @Field("lat") String lat,
            @Field("lng") String lng,
            @Field("ubah_foto") String ubah_foto
    );


    @FormUrlEncoded
    @POST("deleteCafe.php")
    Call<ResponseModel> performHapusCafe(
            @Field("id_cafe") String id_cafe
    );

}
