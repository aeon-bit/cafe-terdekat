package com.irfan.pencariancafe.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.irfan.pencariancafe.models.Users;


public class SessionManager
{
    private static final String USER_DATA = "UserData",
                                IS_LOGIN = "islogin";

    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    public static void init(Context c)
    {
        if (sp == null || editor == null)
        {
            sp = c.getSharedPreferences("smartUser", 0);
            editor = sp.edit();
        }
    }

    public static void login(Users s)
    {
        editor.putString(USER_DATA, Gxon.toJsonObject(s)).commit();
        editor.putBoolean(IS_LOGIN, true).commit();
    }

    public static boolean isLogin()
    {
        return sp.getBoolean(IS_LOGIN, false);
    }

    public static void logout()
    {
        editor.putString(USER_DATA, "").commit();
        editor.putBoolean(IS_LOGIN, false).commit();
    }

    // ------------  -_-_-_-  --------------

    public static String getIdUser()
    {
        return getUserData().getId_user();
    }

    public static String getNamaUser()
    {
        return getUserData().getNama_user();
    }

    public static String getUsername()
    {
        return getUserData().getUsername();
    }

    public static String getPassword()
    {
        return getUserData().getPassword();
    }

    public static String getFoto()
    {
        return getUserData().getFoto();
    }

    public static String getRole()
    {
        return getUserData().getRole();
    }

    public static Users getUserData()
    {
        return Gxon.fromJsonObject(sp.getString(USER_DATA, ""), Users.class);
    }

    public static void setUserData(Users s)
    {
        editor.putString(USER_DATA, Gxon.toJsonObject(s)).commit();
    }
}