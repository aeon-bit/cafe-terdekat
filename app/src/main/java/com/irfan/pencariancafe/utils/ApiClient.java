package com.irfan.pencariancafe.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASEURL = "http://192.168.0.200/cafe_terdekat/";

    public static final String API = BASEURL + "api/";
    public static final String IMAGE_URL = BASEURL + "uploaded/";


    public static Retrofit retrofit = null;


    public static Retrofit getApiClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(API).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}