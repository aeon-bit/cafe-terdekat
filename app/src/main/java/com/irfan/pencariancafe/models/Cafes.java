package com.irfan.pencariancafe.models;

public class Cafes {
    String id_cafe, nama_cafe, alamat, lat, lng, jam_buka, jam_tutup, foto, rating, review, jarak, nama_user;

    public String getId_cafe() {
        return id_cafe;
    }

    public void setId_cafe(String id_cafe) {
        this.id_cafe = id_cafe;
    }

    public String getNama_cafe() {
        return nama_cafe;
    }

    public void setNama_cafe(String nama_cafe) {
        this.nama_cafe = nama_cafe;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getJam_buka() {
        return jam_buka;
    }

    public void setJam_buka(String jam_buka) {
        this.jam_buka = jam_buka;
    }

    public String getJam_tutup() {
        return jam_tutup;
    }

    public void setJam_tutup(String jam_tutup) {
        this.jam_tutup = jam_tutup;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public double getJarakD() {
        return Double.parseDouble(this.jarak);
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }
}
