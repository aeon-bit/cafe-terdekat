package com.irfan.pencariancafe.models;

import com.google.gson.annotations.SerializedName;

public class Users {

    @SerializedName("response")
    private String Response;

    @SerializedName("id_user")
    private String id_user;

    @SerializedName("nama_user")
    private String nama_user;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("foto")
    private String foto;

    @SerializedName("role")
    private String role;

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
