package com.irfan.pencariancafe.models;

import com.google.gson.annotations.SerializedName;

public class Reviews {

    @SerializedName("response")
    private String Response;

    @SerializedName("review")
    private String review;

    @SerializedName("id_user")
    private String id_user;

    @SerializedName("rating")
    private String rating;


    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
